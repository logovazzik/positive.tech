const sumByField = (obj, field) => obj.reduce((previous, current) => previous + current[field], 0);

const mapCurrency = (from, to) =>
    Object.keys(to)
        .reduce((previous, current) => {
                if (from[to[current]]) {
                    return Object.assign({}, previous, {[current]: from[to[current]]});
                }
                return previous;
            },
            {});


const selectedCart = [
    {price: 20},
    {price: 45},
    {price: 67},
    {price: 1305}
];

const currencyEnum = {
    rubles: 'RUB',
    euros: 'EUR',
    USdollars: 'USD',
    pounds: 'GBP',
    yens: 'JPY'
};

const getCurrency = (baseCurrency) => {
	//В идеале тут запрос к api с указанием baseCurrency
    return Promise.resolve(
        {
            'AUD': 1.3214,
            'BGN': 1.7505,
            'BRL': 3.3378,
            'CAD': 1.3231,
            'CHF': 0.97118,
            'CNY': 6.8391,
            'CZK': 23.536,
            'DKK': 6.6564,
            'GBP': 0.78587,
            'HKD': 7.7998,
            'HRK': 6.6258,
            'HUF': 276.68,
            'IDR': 13305.0,
            'ILS': 3.5418,
            'INR': 64.532,
            'JPY': 111.3,
            'KRW': 1137.7,
            'MXN': 18.045,
            'MYR': 4.288,
            'NOK': 8.468,
            'NZD': 1.3734,
            'PHP': 50.226,
            'PLN': 3.7877,
            'RON': 4.0916,
            'RUB': 59.669,
            'SEK': 8.7443,
            'SGD': 1.3869,
            'THB': 33.945,
            'TRY': 3.5116,
            'ZAR': 12.936,
            'EUR': 0.89501
        });
};


const calculateCartByCurrency = (totalAmount, baseCurrency) =>
    getCurrency(baseCurrency)
        .then((rates) =>
            mapCurrency(Object.assign({}, rates, {[baseCurrency]: 1}), currencyEnum))
        .then((currencyObject) =>
            Object.keys(currencyObject)
                .reduce((previous, current) => {
                        previous[current] = currencyObject[current] * totalAmount;
                        return previous;
                    },
                    {}));

calculateCartByCurrency(sumByField(selectedCart, 'price'), currencyEnum.USdollars).then((res) => {
    console.log(res)
});
